(function () {
    'use strict';

    // Http Provider
    function configHttp(
        $httpProvider,
        RestangularProvider
    ) {

        if (data.cachePagesCall) {
            // Caching pages from Restangular
            RestangularProvider.setDefaultHttpFields({cache: true});
        }

        if ( // DEV API
        (  data.credentials_dev &&
            host.indexOf('//dev-api.') != -1 ) ||

        // TEST API
        (  data.credentials_test &&
            host.indexOf('//test-api.') != -1 ) ||

        // ACCEPT && PROD API
        (  data.credentials && (
                host.indexOf('//api.') != -1 ||
                host.indexOf('//accept.') != -1
            )
        )
        ) {

            $httpProvider.defaults.xsrfHeaderName = 'X-XSRF-TOKEN';
            $httpProvider.defaults.xsrfCookieName = 'XSRF-TOKEN';
            $httpProvider.defaults.withCredentials = true;
        }

        if (host.indexOf('//api.') != -1 ||
            host.indexOf('//accept.') != -1) {
            $httpProvider.defaults.xsrfHeaderName = 'X-XSRF-TOKEN';
            $httpProvider.defaults.xsrfCookieName = 'XSRF-TOKEN';
            $httpProvider.defaults.withCredentials = true;
        }

        $httpProvider.useApplyAsync(true);

        // HTTP Headers
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $httpProvider.defaults.headers.post['Accept'] = 'application/json, text/javascript';
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
        $httpProvider.defaults.headers.post['Access-Control-Max-Age'] = '1728000';
        $httpProvider.defaults.headers.common['Access-Control-Max-Age'] = '1728000';
        $httpProvider.defaults.headers.common['Accept'] = 'application/json, text/javascript';
        $httpProvider.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';

        $httpProvider.defaults.useXDomain = true;

        $httpProvider.interceptors.push(function($q, $cookies, $injector) {

            if (data.authenticationAllowed) {
                var $rootScope = $injector.get('$rootScope');
                var localStorage = $injector.get('localStorage');
            }

            return {
                response: function(response) {

                    // Sets the Credentials for Restangular calls when the SecuryToken Cookie is set.
                    if ($cookies.get('security_token')) {
                        RestangularProvider.setDefaultHttpFields({
                            withCredentials: true
                        });
                    }

                    if (data.authenticationAllowed) {
                        $rootScope.$watch('isLogged', function () {
                            if ($rootScope.isLogged) {
                                $httpProvider.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getLocalStorage('access_token');
                            }
                        });

                        if ($rootScope.isLogged) {
                            $httpProvider.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getLocalStorage('access_token');
                        }

                        if (data.credentials) {
                            $httpProvider.defaults.headers.common['X-XSRF-TOKEN'] = $cookies.get('XSRF-TOKEN');
                        }

                        $httpProvider.defaults.headers.common['X-CSRF-TOKEN'] = $cookies.get('XSRF-TOKEN');
                    }

                    return response;
                }
            }
        });

    }

    configHttp.$inject =[
        '$httpProvider',
        'RestangularProvider'
    ];

    angular
        .module('bravoureAngularApp')
        .config(configHttp);

})();
