## Angular Config Http Service / Bravoure component

This Component configs $http in angular (i18n files).

### **Versions:**

1.0.5 - Adding cachePagesCall option
1.0.4 - Adding authenticationAllow option
1.0.3 - Caching pages for Restangular
1.0.2 - Credentials added to Restangular calls when the Secury Token Cookie is set.
1.0.1 - Fixed bug related to it
1.0.0 - Initial Stable version

---------------------
